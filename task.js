while (true) {
  let firstName = prompt("What's the student first name?", "Stan");
  let lastName = prompt("What's the student last name?", "Ost");
  let mark = +prompt("What's the student mark?");
  let repeater;
  let convertToLetter;

  if (mark >= 0 && mark <= 100) {
    if (mark >= 95 && mark <= 100) {
      convertToLetter = "A";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 90 && mark <= 94) {
      convertToLetter = "A-";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 85 && mark <= 89) {
      convertToLetter = "B+";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 80 && mark <= 84) {
      convertToLetter = "B";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 75 && mark <= 79) {
      convertToLetter = "B-";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 70 && mark <= 74) {
      convertToLetter = "C+";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 65 && mark <= 69) {
      convertToLetter = "C";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 60 && mark <= 64) {
      convertToLetter = "C-";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 55 && mark <= 59) {
      convertToLetter = "D+";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 50 && mark <= 54) {
      convertToLetter = "D";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 25 && mark <= 49) {
      convertToLetter = "E";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 0 && mark <= 24) {
      convertToLetter = "F";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    }
  } else {
    console.log("Please enter correct info!");
  }
}
